<?php $id="top";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<!-- main -->
<div class="p-top">
    <div class="c-slide1 c-banner">
        <?php for($i = 0; $i < 5; $i++) { ?>
        <div class="c-banner__item">
            <img src="./assets/image/top/slide1.jpg" alt="">
            <span>
                キャッチコピー。キャッチコピー。<br/>
                キャッチコピー。キャッチコピー。
            </span>
        </div>
        <?php } ?>
    </div>
    <main class="l-main">
        <!-- p-top1 -->
        <section class="p-top1">
            <div class="c-news">
                <div class="c-news__title">
                    <p>NEWS & TOPICS</p>
                    <span>新着情報</span>
                    JA鹿追町からのお知らせ、トピックスをお届けいたします。
                </div>
                <ul class="c-list1">
                    <li>
                        <a href="#">
                            <span class="c-list1__cal">2017.12.20</span>
                            <span class="c-text1 c-text1--c1">お知らせ</span>
                            <span class="c-news__text">お知らせのタイトルを表示。</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="c-list1__cal">2017.12.20</span>
                            <span class="c-text1 c-text1--c2">トピックス</span>
                            <span class="c-news__text">トピックスのタイトルを表示。</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="c-list1__cal c-list1__cal--r1">2017.12.20</span>
                            <span class="c-text1 c-text1--c2">トピックス</span>
                            <span class="c-news__text">トピックスのタイトルを表示。</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="c-list1__cal c-list1__cal--r2">2017.12.20</span>
                            <span class="c-text1 c-text1--c2">トピックス</span>
                            <span class="c-news__text">トピックスのタイトルを表示。</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="c-list1__cal">2017.12.20</span>
                            <span class="c-text1 c-text1--c3">更新</span>
                            <span class="c-news__text">更新した内容タイトルを表示。</span>
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <!-- p-top2 -->
        <section class="p-top2">
            <div class="c-title1">
                <p>安心、安全  鹿追ブランド</p>
                Shikaoi Brand
            </div>
            <div class="c-list2">
                <div class="c-list2__box">
                    <a href="#">
                        <img src="./assets/image/top/brand1.jpg" alt="">
                        <div class="c-list2__content">
                            <p>肥沃な大地で生産する安全な農作物</p>
                            テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                        </div>
                    </a>
                </div>
                <div class="c-list2__box">
                    <a href="#">
                        <img src="./assets/image/top/brand2.jpg" alt="">
                        <div class="c-list2__content u-ml-1">
                            <p>北海道を代表する高品質の牛乳</p>
                            テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                        </div>
                    </a>
                    
                </div>
                <div class="c-list2__box">
                    <a href="#">
                        <img src="./assets/image/top/brand3.jpg" alt="">
                        <div class="c-list2__content u-ml-1">
                            <p class="u-m0">信頼の「鹿追ブランド」牛肉・豚肉</p>
                            テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                        </div>
                    </a>
                </div>
            </div>
        </section>
        <!-- p-top3 -->
        <section class="p-top3">
            <div class="c-title1">
                <p>青年部・女性部・熟年会</p>
                Shikaoi Community
            </div>
            <div class="c-list2">
                <div class="c-list2__box">
                    <a href="#">
                        <img src="./assets/image/top/comm1.jpg" alt="">
                        <div class="c-list2__content">
                            <p>青年部</p>
                            テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                        </div>
                        <div class="c-list2__tag">
                            ダミー写真
                        </div>
                    </a>
                </div>
                <div class="c-list2__box u-ml2">
                    <a href="#">
                        <img src="./assets/image/top/comm2.jpg" alt="">
                        <div class="c-list2__content">
                            <p>女性部</p>
                            テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                        </div>
                        <div class="c-list2__tag c-list2__tag--l1">
                            ダミー写真
                        </div>
                    </a>
                </div>
                <div class="c-list2__box">
                    <a href="#">
                        <img src="./assets/image/top/comm3.jpg" alt="">
                        <div class="c-list2__content">
                            <p>熟年会</p>
                            テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
                        </div>
                        <div class="c-list2__tag c-list2__tag--l2">
                            ダミー写真
                        </div>
                    </a>
                </div>
            </div>
        </section>
        <!-- p-top4 -->
        <section class="p-top4">
            <div class="c-title1">
                <p>フォトギャラリー</p>
                Photo Gallery
            </div>
            <div class="c-gallery">
                <a href="#"><img src="./assets/image/top/gallery1.jpg" alt=""></a>
                <a href="#"><img src="./assets/image/top/gallery2.jpg" alt=""></a>
                <a href="#"><img src="./assets/image/top/gallery3.jpg" alt=""></a>
            </div>
        </section>
        <!-- p-top5 -->
        <section class="p-top5">
            <div class="c-title1">
                <p>インフォメーション</p>
                Information
            </div>
            <div class="c-list3">
                <div class="c-list3__box">
                    <a href="#">
                        <img src="./assets/image/top/info1.jpg" alt="">
                        <div class="c-list3__content">
                            <p>Aコープ鹿追店</p>
                            <p>国産野菜統一宣言！ 地産地消をおいしく応援中。</p>
                            <span>
                                Ａコープ鹿追店は生産者と消費者から信頼される店舗を目指し、<br/>
                                安心で安全な食品をご提供いたします。
                            </span>
                        </div>
                        <img src="./assets/image/top/arrow1.png" alt="" class="c-list3__arrow1">
                    </a>
                </div>
                <div class="c-list3__box">
                    <a href="#">
                        <img src="./assets/image/top/info2.jpg" alt="">
                        <div class="c-list3__content">
                            <p>ふるさと納税返礼品［鹿追町］</p>
                            <p>JA鹿追町のふるさとチョイス。</p>
                            <span>
                                安心、安全な鹿追産の牛肉、豚肉、ハンバーグをご提供いたします。<br/>
                                是非ご利用ください。
                            </span>
                        </div>
                        <img src="./assets/image/top/arrow1.png" alt="" class="c-list3__arrow1">
                    </a>
                </div>
                <div class="c-list3__box">
                    <a href="#">
                        <img src="./assets/image/top/info3.jpg" alt="">
                        <div class="c-list3__content">
                            <p>
                                農業求人情報
                                <span class="c-list3__tag c-list3__tag--tl1">NEW</span>
                            </p>
                            <p>酪農スタッフ募集のお知らせ。</p>
                            <span>
                                酪農スタッフを募集しています。<br/>
                                広大な十勝平野で私たちと一緒に働きませんか！
                            </span>
                        </div>
                        <img src="./assets/image/top/arrow1.png" alt="" class="c-list3__arrow1">
                    </a>
                </div>
                <div class="c-list3__box">
                    <a href="#">
                        <img src="./assets/image/top/info4.jpg" alt="">
                        <div class="c-list3__content">
                            <p>
                                とっておきのレシピ
                                <span class="c-list3__tag c-list3__tag--tl2">NEW</span>
                            </p>
                            <p>野菜の大根おろしあえ vol.89</p>
                            <span>
                                旬の野菜を大根おろしであえ特性タレをかけました。<br/>
                                ごはんのおかずにもビールのおつまににも良く合います。
                            </span>
                        </div>
                        <img src="./assets/image/top/arrow1.png" alt="" class="c-list3__arrow1 c-list3__arrow1--t1">
                    </a>
                </div>
                <div class="c-list3__box">
                    <a href="#">
                        <img src="./assets/image/top/info5.jpg" alt="">
                        <div class="c-list3__content">
                            <p>JA通信しかおい</p>
                            <p>JA通信11月号を公開しました。</p>
                            <span>
                                農業ニュースや各種イベントなど楽しい話題をお届けいたします。<br/>
                                鹿追町内の皆さまへ毎月無料配布をおこなっています。
                            </span>
                        </div>
                        <img src="./assets/image/top/arrow1.png" alt="" class="c-list3__arrow1">
                    </a>
                </div>
            </div>
        </section>
    </main>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>