<!-- ＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊ -->
<!-- ./footer -->
<footer>
    <div class="c-footer">
        <div class="c-footer__left">
            <p>JA鹿追町　〒081-0341 北海道河東郡鹿追町新町4丁目51番地 </p>
            <p>tel. <a href="tel:+0156-66-2131">0156-66-2131</a>   fax. 0156-66-3194</p>
            <ul>
                <li><a href="#">アクセスマップ</a></li>
                <li><a href="#">お問い合わせ</a></li>
            </ul>
        </div>
        <div class="c-footer__right">
            個人情報保護方針の取り扱い | 貯金苦情受付 | 共済苦情受付 |  サイトマップ  | リンク
        </div>
    </div>
</footer>
</body>
</html>