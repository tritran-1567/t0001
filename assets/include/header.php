<!DOCTYPE html>
<html lang="ja" id="pagetop">
    <head>
        <meta charset="UTF-8">
        <meta name="format-detection" content="telephone=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
        <link href="/assets/css/style.css" rel="stylesheet">
        <link href="/assets/js/slick/slick.css" rel="stylesheet">
        <link href="/assets/js/slick/slick-theme.css" rel="stylesheet">
        <script src="/assets/js/jquery-2.2.4.min.js"></script>
        <script src="/assets/js/slick/slick.js"></script>
        <script src="/assets/js/functions.js"></script>
    </head>
	<body class="page-<?php echo $id; ?>">
	<!-- ./header -->
        <header>
			<div class="c-headertop">
				<div class="c-headertop__inner">
					平成29年度スローガン「農〜 魅せる〜」
				</div>
			</div>
			<div class="c-header">
				<div class="c-header__inner">
					<div class="c-header__logo">
						<a href="/">
							<img src="./assets/image/top/logo1.png" alt="">
						</a>
					</div>
					<nav class="c-navi1">
						<ul>
							<li><a href="/top.php">ホーム</a></li>
							<li><a href="/about.php">JA鹿追町について</a></li>
							<li><a href="/nogyo.php">鹿追町の農業</a></li>
							<li><a href="#">青年部・女性部・熟年会</a></li>
							<li><a href="#">職場紹介</a></li>
							<li><a href="#">組合員情報</a></li>
							<li><a href="#">農業求人</a></li>
							<li><a href="#">新着情報</a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>
		<!-- ＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊　＊ -->
		