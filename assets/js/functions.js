/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u){
  return {
    Tablet:(u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1)
      || u.indexOf("ipad") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
      || u.indexOf("kindle") != -1
      || u.indexOf("silk") != -1
      || u.indexOf("playbook") != -1,
    Mobile:(u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
      || u.indexOf("iphone") != -1
      || u.indexOf("ipod") != -1
      || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
      || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
      || u.indexOf("blackberry") != -1
  }
})(window.navigator.userAgent.toLowerCase());

if(_ua.Tablet){
  $("meta[name='viewport']").attr('content', 'width=1100');
}

$(document).ready(function(){
  $(".c-slide1").slick({
    dots: true
  });

  // add class
  if($("body").hasClass("page-top")){
    $(".c-navi1 ul li:first-child").addClass("active");
  } else if($("body").hasClass("page-about")){
    $(".c-navi1 ul li:nth-child(2)").addClass("active");
  }else{
    $(".c-navi1 ul li:nth-child(3)").addClass("active");
  }

  // hover menu
  $(".c-navi1 ul li").hover(
    function(){
      $(".c-navi1 ul li").removeClass("active");
      $(this).addClass("active");
    },
    function(){
      $(".c-navi1 ul li").removeClass("active");
    }
  );
});