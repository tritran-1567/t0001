<?php $id="about";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>
<!-- main -->
<div class="p-about">
    <main class="l-main">
        <!-- p-about1 -->
        <section class="p-about1">
            <div class="c-title2">
                <p>組合長からのご挨拶</p>
                感謝の思いを込めて
            </div>
            <div class="c-intro">
                <img src="/assets/image/about/avt1.jpg" alt="">
                <div class="c-intro__content">
                    <p>
                        　当ＪＡは、農村の民主化と社会的経済的地位の向上を目指して、昭和23年に設立を致しました。<br/>
                        思えばこの自然環境の美しい鹿追町も我々の先達が困苦欠乏に耐え忍び乍らも、未来に楽土を夢み開拓に汗、血を
                        流したのであります。当時は一部の営利商人や金融家の手により流通機構が形成され、中には財産の全てを業者に
                        依存せねばならない農家も少なくはなく、 これらの重圧から経営を守る組織として産業組合が設立され、農業協
                        同組合へと引き継がれ幾多の試練を乗り越え今日に至りました。
                    </p>
                    <p class="c-intro__content--l1">
                        　今日では想いも及ばない事ではありますが、これらの教訓と歴史を礎に現在のＪＡが生き抜いてこられたことであ<br/>
                        り、 農家組合員そして地域社会の中で生業させて頂いていることに対し深く感謝の念を申し上げます。<br/>
                        昨今の社会・経済の環境変化が著しい中、当ＪＡが地域社会のリーダーとして理解され親しまれる様、 また、今<br/>
                        後も巾広く貢献出来る様に事業を推めてまいりますので、皆様のご利用・ご協力を頂きたいと存ずる次第です。
                    </p>
                    <span>
                        ＪＡ鹿追町　代表理事組合長　木幡浩喜
                    </span>
                </div>
            </div>
        </section>
        <!-- p-about2 -->
        <section class="p-about2">
            <div class="c-title2">
                <p>ＪＡ綱領</p>
                わたしたちＪＡのめざすもの
            </div>
            <div class="c-program__img">
                <img src="./assets/image/about/about1.jpg" alt="">
                <div class="c-tag1">
                    社屋外観写真
                </div>
            </div>
            <div class="c-program">
                <p>
                    　わたしたちＪＡの組合員・役職員は、協同組合運動の基本的な定義・価値・原則（自主、自立、参加、民主的運営、公正、連帯等）に基づき行動します。<br/>
                    そして、地球的視野に立って環境変化を見通し、組織・事業・経営の革新を図ります。さらに、地域・全国・世界の協同組合の仲間と連携し、より民主的で公正な社会の実<br/>
                    現に努めます。このため、わたしたちは次のことを通じ、農業と地域社会に根ざした組織としての社会的役割を誠実に果たします。
                </p>
                <p>
                    わたしたちは、<br/>
                    一、地域の農業を振興し、わが国の食と緑と水を守ろう。<br/>
                    一、環境・文化・福祉への貢献を通じて、安心して暮らせる豊かな地域社会を築こう。<br/>
                    一、ＪＡへの積極的な参加と連帯によって、協同の成果を実現しよう。<br/>
                    一、自主・自立と民主的運営の基本に立ち、ＪＡを健全に経営し信頼を高めよう。<br/>
                    一、協同の理念を学び実践を通じて、共に生きがいを追求しよう。
                </p>
            </div>
        </section>
        <!-- p-about3 -->
        <section class="p-about3">
            <div class="c-title2">
                <p>基本理念</p>
                当ＪＡは４つの基本理念の下、事業展開を行います。
            </div>
            <div class="c-idea">
                <div class="c-idea__content">
                    　JAは、人々が連帯し助け合うことを意味する「相互扶助」の精神のもとに、組合員農家の農業経営と生活を守り、より良い地域社会を築くことを<br/>
                    目的につくられた協同組合です。JA鹿追町は、次の基本理念の下事業を展開します。
                </div>
                <ul class="c-idea__list">
                    <li>
                        <p>1. 真に農協らしい農協</p>
                        <span>組合員のための事業展開（組合員の経済・生活上必要な事業であっても決して農協のためであってはならない）</span>
                    </li>
                    <li>
                        <p>2. 正確な情報を正しく提供（公開）出来る農協</p>
                       <span>
                            農協の経営状況・財務等を正しく公開するとともに、農業情勢および営農上必要とする情報を正しく伝え、組合員が適正な判断が出来る様に<br/>
                            します
                        </span>
                    </li>
                    <li>
                        <p>３. 組合員が結集出来る農協</p>
                        <span>
                            組合員が必要とする農協。組合員の意見を積極的に聞くとともに動向を把握し事業展開を行う。理念だけではなく、他に負けない実利伴う購<br/>
                            買・販売事業と組合員に応える利用事業及び農業支援システムの展開
                        </span>
                    </li>
                    <li>
                        <p>4. 地域住民に応える農協</p>
                        <span>
                            農家組合員だけでなく、地域の住民に対し金融・共済事業、給油所、整備工場や生活店舗を含む事業により貢献する農協
                        </span>
                    </li>
                </ul>
            </div>
        </section>
        <!-- p-about4 -->
        <section class="p-about4">
            <div class="c-title2">
                <p>組織</p>
                概況
            </div>
            <div class="c-organ">
                <div class="c-organ__left">
                    <p>（設立）昭和23年3月1日</p>
                    <p>（組合員）正組合員 248名　法人 31名　准組合員 997名</p>
                    <div class="c-organ__wrapper">
                        <p>（役員）</p>
                        <ul class="u-ml-1">
                            <li>理事 14名　監事 5名</li>
                            <li>代表理事組合長 1名</li>
                            <li>専務理事 1名</li>
                            <li>常勤監事 1名</li>
                            <li>非常勤理事 1名</li>
                            <li>非常勤監事 4名</li>
                            <li class="u-pl50">計 19名</li>
                        </ul>
                    </div>
                    <div class="c-organ__wrapper">
                        <p>（職員）</p>
                        <ul>
                            <li>一般職員 76名（男）・33名（女）　計 109名</li>
                            <li>工場技術員 17名（男）計 17名</li>
                            <li class="u-pl45">計 93名（男）・33名（女）</li>
                        </ul>
                    </div>
                </div>
                <div class="c-organ__right">
                    <div class="c-organ__wrapper">
                        <p>（出資金）</p>
                        <ul>
                            <li>総口数 272,924口</li>
                            <li>総額 1,264,620千円</li>
                            <li>一口金額 5,000円</li>
                            <li>最高限度 10,000口</li>
                        </ul>
                    </div>
                    <div class="c-organ__wrapper">
                        <p>（運営）</p>
                        <ul>
                            <li>総会 − 毎年5月開催（通常）</li>
                            <li>理事会 − 毎月1回（定例）</li>
                            <li>監査 − 4半期毎（定例）</li>
                        </ul>
                    </div>
                    <div class="c-organ__wrapper">
                        <p>（外かく組織）</p>
                        <ul class="u-ml15">
                            <li>◇ 酪農事業推進部会 − 16名</li>
                            <li>◇ 酪農事業推進部会 − 16名</li>
                            <li>◇ 畜産事業推進部会 − 4名</li>
                            <li>◇ 作業受委託事業推進部会 − 16名</li>
                            <li>◇ 女性の会 − 36名</li>
                            <li>◇ JA青年部 − 83名</li>
                            <li>
                                ◇ JA女性部 − 141名<br/>
                                （フレッシュミズ会員54名・喜楽会員50名）
                            </li>
                            <li>◇ 熟年会 − 29名</li>
                            <li>◇ 年金友の会 − 901名</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- p-about5 -->
        <section class="p-about5">
            <div class="c-title2">
                <p>組織機構図</p>
                フローチャート
            </div>
            <div class="c-organ__img">
                <img src="./assets/image/about/about2.jpg" alt="">
                <div class="c-tag1">
                    組織機構図画像
                </div>
            </div>
        </section>
        <!-- p-about6 -->
        <section class="p-about6">
            <div class="c-title2">
                <p>職員募集のお知らせ</p>
                Recruitment
            </div>
            <div class="c-recuit">
                <div class="c-recuit__content">
                    <p>
                        JA鹿追町では職員の募集を行っています。<br/>
                        2018年4月採用 1〜3名を予定しています。
                    </p>
                    <a href="#">
                        募集要項はこちら
                        <img src="./assets/image/about/arrow1.png" alt="" class="u-mt5">
                    </a>
                </div>
                <div class="c-recuit__content">
                    <img src="./assets/image/about/about3.jpg" alt="">
                    <div class="c-tag1">
                        職場の写真
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>